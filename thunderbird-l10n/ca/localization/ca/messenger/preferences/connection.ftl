# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

connection-dns-over-https-url-resolver = Utilitza el proveïdor
    .accesskey = r
# Variables:
#   $name (String) - Display name or URL for the DNS over HTTPS provider
connection-dns-over-https-url-item-default =
    .label = { $name } (per defecte)
    .tooltiptext = Utilitza l'URL per defecte per resoldre DNS sobre HTTPS
connection-dns-over-https-url-custom =
    .label = Personalitzat
    .accesskey = P
    .tooltiptext = Introduïu el vostre URL preferit per resoldre DNS sobre HTTPS
connection-dns-over-https-custom-label = Personalitzat
