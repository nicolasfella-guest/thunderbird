# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### These strings are used inside the Accessibility panel.

accessibility-learn-more = Lär dig mer
accessibility-text-label-header = Textetiketter och namn

## Text entries that are used as text alternative for icons that depict accessibility isses.

accessibility-warning =
    .alt = Varning
accessibility-fail =
    .alt = Fel
accessibility-best-practices =
    .alt = Bästa praxis

## Text entries for a paragraph used in the accessibility panel sidebar's checks section
## that describe that currently selected accessible object has an accessibility issue
## with its text label or accessible name.

accessibility-text-label-issue-area = Använd attributet <code>alt</code> för att namnge element <div>area</div> som har attributet <span>href</span>. <a>Läs mer</a>
accessibility-text-label-issue-dialog = Dialoger ska namnges. <a>Läs mer</a>
accessibility-text-label-issue-document-title = Dokument måste ha en <code>titel</code>. <a>Läs mer</a>
accessibility-text-label-issue-embed = Inbäddat innehåll måste märkas med namn. <a>Läs mer</a>
accessibility-text-label-issue-figure = Siffror med valfria bildtexter ska märkas med namn. <a>Läs mer</a>
accessibility-text-label-issue-fieldset = Elementet <code>fieldset</code> måste märkas med namn. <a>Läs mer</a>
accessibility-text-label-issue-fieldset-legend = Använd elementet <code>legend</code> för att märka elementet <span>fieldset</span>. <a>Läs mer</a>
accessibility-text-label-issue-form = Element Form måste vara märkt med namn. <a>Läs mer</a>
accessibility-text-label-issue-form-visible = Elementet Form ska ha en synlig textetikett. <a>Läs mer</a>
accessibility-text-label-issue-frame = Elementet <code>frame</code> måste märkas med namn. <a>Läs mer</a>
accessibility-text-label-issue-glyph = Använd attributet <code>alt</code> för att namnge elementet <span>mglyph</span>. <a>Läs mer</a>
accessibility-text-label-issue-heading = Rubriker måste namnges. <a>Läs mer</a>
accessibility-text-label-issue-heading-content = Rubriker ska ha synligt textinnehåll. <a>Läs mer</a>
accessibility-text-label-issue-iframe = Använd attributet <code>title</code> för att beskriva innehållet i <span>iframe</span>. <a>Läs mer</a>
accessibility-text-label-issue-image = Innehåll med bilder måste markeras med namn. <a>Läs mer</a>
accessibility-text-label-issue-interactive = Interaktiva element måste märkas med namn. <a>Läs mer</a>
accessibility-text-label-issue-optgroup-label = Använd attributet <code>label</code> för att märka elementet <span>optgroup</span>. <a>Läs mer</a>
accessibility-text-label-issue-toolbar = Verktygsfält måste namnges när det finns mer än ett verktygsfält. <a>Läs mer</a>
