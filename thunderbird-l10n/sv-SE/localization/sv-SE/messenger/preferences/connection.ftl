# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

connection-dns-over-https-url-resolver = Använd leverantör
    .accesskey = A
# Variables:
#   $name (String) - Display name or URL for the DNS over HTTPS provider
connection-dns-over-https-url-item-default =
    .label = { $name } (Standard)
    .tooltiptext = Använd standardadressen för att lösa DNS över HTTPS
connection-dns-over-https-url-custom =
    .label = Anpassad
    .accesskey = A
    .tooltiptext = Ange önskad adress för att lösa DNS över HTTPS
connection-dns-over-https-custom-label = Anpassad
