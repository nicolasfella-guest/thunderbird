# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

connection-dns-over-https-url-custom =
    .label = Udmawan
    .accesskey = U
    .tooltiptext = Sekcem tansa-ik tudmawant tufrint akken ad tefruḍ DNS s HTTPS
connection-dns-over-https-custom-label = Udmawan
