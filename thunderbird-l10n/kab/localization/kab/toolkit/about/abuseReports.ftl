# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

abuse-report-title-extension = Mmel asiɣzef-a i { -vendor-short-name }
abuse-report-title-theme = Mmel asentel-a i { -vendor-short-name }
abuse-report-subtitle = D acu i d ugur?
# Variables:
#   $author-name (string) - Name of the add-on author
abuse-report-addon-authored-by = sɣur <a data-l10n-name="author-name">{ $author-name }</a>
abuse-report-submit-description = Seglem ugur( d afrayan)

## Panel buttons.

abuse-report-cancel-button = Sefsex
abuse-report-next-button = Ɣer zdat
abuse-report-goback-button = Uɣal
abuse-report-submit-button = Azen

## Message bars descriptions.


## Variables:
##   $addon-name (string) - Name of the add-on

abuse-report-messagebar-submitting = Tuzna n uneqqis ɣef <span data-l10n-name="addon-name">{ $addon-name }</span>.
abuse-report-messagebar-submitted = Tanmirt ɣef uneqqis id-tuzneḍ. Tebɣiḑ ad tekkseḍ <span data-l10n-name="addon-name">{ $addon-name }</span> ?
abuse-report-messagebar-submitted-noremove = Tanmirt ɣef uneqqis id-tuzneḍ.
abuse-report-messagebar-removed-extension = Tanmirt ɣef uneqqis id-tuzneḍ. Tekkseḍ asiɣzef <span data-l10n-name="addon-name">{ $addon-name }</span>.
abuse-report-messagebar-removed-theme = Tanmirt ɣef uneqqis id-tuzneḍ. Tekkseḍ asentel <span data-l10n-name="addon-name">{ $addon-name }</span>.
abuse-report-messagebar-error = Tella tucḍa deg uzzna n uneqqis i <span data-l10n-name="addon-name">{ $addon-name }</span>.
abuse-report-messagebar-error-recent-submit = Aneqqis n <span data-l10n-name="addon-name">{ $addon-name }</span> ur yettwazen ara acku aneqqis nniḍen yettwazen melmi kan.

## Message bars actions.

abuse-report-messagebar-action-remove-extension = Ih, kkes-it
abuse-report-messagebar-action-keep-extension = Uhu, eǧǧ-it
abuse-report-messagebar-action-remove-theme = Ih, kkes-it
abuse-report-messagebar-action-keep-theme = Uhu eǧǧ-it
abuse-report-messagebar-action-retry = Ɛreḍ i tikelt-nniḍen
abuse-report-messagebar-action-cancel = Sefsex

## Abuse report reasons (optionally paired with related examples and/or suggestions)

abuse-report-damage-reason = Yessexsaṛ aselkim-iwakked isefka-iw
abuse-report-spam-reason = Yesnulfu-d ispamen d udellel
abuse-report-spam-example = Amedya: Sekcem adellel deg yisebtar web
abuse-report-settings-suggestions-search = Snifel iɣewwaṛen n  unadi amezwer
abuse-report-settings-suggestions-homepage = Snifel asebter agejdan akked yiccer amaynut
abuse-report-broken-reason-theme = UR iteddu ara neɣ iseḥbes askan n yiminig
abuse-report-policy-reason = Agbur n kaṛuh, n tekriḍt neɣ arusḍif
abuse-report-unwanted-reason = Werǧin bɣiɣ asiɣzef-agi yerna ur zmireɣ ad t-kkseɣ
abuse-report-other-reason = Ayen nniḍen
