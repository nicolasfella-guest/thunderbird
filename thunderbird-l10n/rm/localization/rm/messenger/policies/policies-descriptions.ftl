# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## The Enterprise Policies feature is aimed at system administrators
## who want to deploy these settings across several Thunderbird installations
## all at once. This is traditionally done through the Windows Group Policy
## feature, but the system also supports other forms of deployment.
## These are short descriptions for individual policies, to be displayed
## in the documentation section in about:policies.

policy-3rdparty = Definir directivas a las qualas WebExtensions pon acceder via chrome.storage.managed.
policy-AppUpdateURL = Definir in URL persunalisà per actualisar l'applicaziun.
policy-Authentication = Configurar l'autentificaziun integrada per las websites che la sustegnan.
policy-BlockAboutAddons = Bloccar l'access a l'administraziun da supplements (about:addons).
policy-BlockAboutConfig = Bloccar l'access a la pagina about:config.
policy-BlockAboutProfiles = Bloccar l'access a la pagina about:profiles.
policy-BlockAboutSupport = Bloccar l'access a la pagina about:support.
policy-CaptivePortal = Activar u deactivar il sustegn da captive portal.
policy-CertificatesDescription = Agiuntar certificats u utilisar certificats integrads.
policy-Cookies = Permetter u scumandar a websites da definir cookies.
policy-DefaultDownloadDirectory = Definir l'ordinatur da standard per telechargiadas.
policy-DisableAppUpdate = Impedir che { -brand-short-name } vegnia actualisà.
policy-DisableDeveloperTools = Bloccar l'access als utensils per sviluppaders.
policy-DisableFeedbackCommands = Deactivar ils cumonds per trametter resuns en il menu d'agid (Trametter in resun e Rapportar ina pagina che engiona).
policy-DisableForgetButton = Impedir l'access al buttun Emblidar.
policy-DisableMasterPasswordCreation = Sche activà, èsi impussibel da crear in pled-clav universal.
policy-DisableProfileImport = Deactivar il cumond da menu per importar datas dad in'autra applicaziun.
policy-DisableSafeMode = Deactivar la funcziun da reaviar en il modus segirà. Remartga: la tasta per maiusclas che po servir per entrar en il modus segirà po mo vegnir deactivada en Windows cun agid da directivas da gruppa (Group Policy).
policy-DisableSecurityBypass = Impedir che l'utilisader ignoreschia tscherts avis da segirezza.
policy-DisableSystemAddonUpdate = Impedir che { -brand-short-name } installeschia ed actualiseschia supplements dal sistem.
policy-DisableTelemetry = Deactivar la telemetria.
policy-DisplayMenuBar = Mussar la trav da menu sco standard.
policy-DNSOverHTTPS = Configurar DNS via HTTPS.
policy-DontCheckDefaultClient = Impedir ch'i vegnia controllà tgenin ch'è il program da standard durant aviar.
policy-DownloadDirectory = Definir l'ordinatur per telechargiadas ed impedir ulteriuras modificaziuns.
# “lock” means that the user won’t be able to change this setting
policy-EnableTrackingProtection = Activar u deactivar la bloccada da cuntegn e tut tenor basegn impedir la modificaziun da l'opziun.
# A “locked” extension can’t be disabled or removed by the user. This policy
# takes 3 keys (“Install”, ”Uninstall”, ”Locked”), you can either keep them in
# English or translate them as verbs.
policy-Extensions = Installar, deinstallar u bloccar extensiuns. L'opziun «Installar» pretenda URLs u percurs sco parameters. Las opziuns «Deinstallar» e «Bloccar» acceptan IDs dad extensiuns.
policy-ExtensionUpdate = Activar u deactivar actualisaziuns automaticas dad extensiuns.
policy-HardwareAcceleration = Sche «false», deactivar l'acceleraziun cun agid da la hardware.
policy-InstallAddonsPermission = Permetter a tschertas websites dad installar supplements.
policy-LocalFileLinks = Permetter a websites specificas dad utilisar colliaziuns a datotecas localas.
policy-NetworkPrediction = Activar u deactivar «network prediction» (prelectura DNS).
policy-OfferToSaveLogins = Applitgar ils parameters che permettan a { -brand-short-name } da memorisar infurmaziuns d'annunzia. Omaduas valurs, «true» e «false», èn validas.
policy-OverrideFirstRunPage = Surscriver la pagina che vegn mussada suenter avair avià l'emprima giada. Definir questa directiva cun ina valur vida per deactivar la pagina.
policy-OverridePostUpdatePage = Surscriver la pagina «Novaziuns» che vegn mussada suenter actualisaziuns. Definir questa directiva cun ina valur vida per impedir che la pagina vegnia mussada.
policy-Preferences = Definir e fixar la valur per ina subgruppa da preferenzas.
policy-PromptForDownloadLocation = Dumandar nua memorisar datotecas cun telechargiar.
policy-Proxy = Configurar ils parameters da proxy.
policy-RequestedLocales = Definir, en la successiun da preferenza, la glista da las linguas («locales») dumandadas da l'applicaziun.
policy-SanitizeOnShutdown2 = Stizzar las datas da navigaziun cun terminar.
policy-SearchEngines = Configurar ils parameters da maschinas da tschertgar. Questa directiva è mo disponibla en la versiun Extended Support Release (ESR).
# For more information, see https://developer.mozilla.org/en-US/docs/Mozilla/Projects/NSS/PKCS11/Module_Installation
policy-SecurityDevices = Installar moduls PKCS #11.
policy-SSLVersionMax = Definir la versiun maximala da SSL.
policy-SSLVersionMin = Definir la versiun minimala da SSL.
policy-SupportMenu = Agiuntar in element da menu persunalisà en il menu d'agid.
# “format” refers to the format used for the value of this policy.
policy-WebsiteFilter = Impedir l'access a tschertas websites. Consultar la documentaziun per ulteriuras infurmaziuns davart il format.
