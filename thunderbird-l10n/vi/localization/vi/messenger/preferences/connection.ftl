# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

connection-dns-over-https-url-resolver = Sử dụng nhà cung cấp
    .accesskey = r
# Variables:
#   $name (String) - Display name or URL for the DNS over HTTPS provider
connection-dns-over-https-url-item-default =
    .label = { $name } (Mặc định)
    .tooltiptext = Sử dụng URL mặc định để xử lý DNS qua HTTPS
connection-dns-over-https-url-custom =
    .label = Tùy chỉnh
    .accesskey = C
    .tooltiptext =
        Nhập URL ưa thích của bạn để giải quyết DNS qua HTTPS
        Nhập URL ưa thích của bạn để giải quyết DNS qua HTTPS
        Nhập URL ưa thích của bạn để giải quyết DNS qua HTTPS
        Nhập URL ưa thích của bạn để xử lí DNS qua HTTPS
connection-dns-over-https-custom-label = Tùy chỉnh
