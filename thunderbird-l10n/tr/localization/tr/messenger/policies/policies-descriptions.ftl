# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## The Enterprise Policies feature is aimed at system administrators
## who want to deploy these settings across several Thunderbird installations
## all at once. This is traditionally done through the Windows Group Policy
## feature, but the system also supports other forms of deployment.
## These are short descriptions for individual policies, to be displayed
## in the documentation section in about:policies.

policy-3rdparty = WebExtension’ların chrome.storage.managed aracılığıyla ulaşabileceği ilkeleri ayarla.
policy-AppUpdateURL = Özel uygulama güncelleme URL’sini ayarla.
policy-Authentication = Destekleyen web siteleri için bütünleşik kimlik doğrulamasını yapılandır.
policy-BlockAboutAddons = Eklenti Yöneticisi'ne (about:addons) erişimi engelle.
policy-BlockAboutConfig = about:config sayfasına erişimi engelle.
policy-BlockAboutProfiles = about:profiles sayfasına erişimi engelle.
policy-BlockAboutSupport = about:support sayfasına erişimi engelle.
policy-CertificatesDescription = Sertifika ekle veya yerleşik sertifikaları kullan.
policy-Cookies = Web sitelerinin çerez yerleştirmesine izin ver veya engelle.
policy-DefaultDownloadDirectory = Varsayılan indirme klasörünü ayarla.
policy-DisableAppUpdate = { -brand-short-name } uygulamasının güncellenmesini engelle.
policy-DisableDeveloperTools = Geliştirici araçlarına erişimi engelle.
policy-DisableFeedbackCommands = Yardım menüsünden geri bildirim göndermeye olanak sağlayan komutları (“Geri bildirim gönder” ve “Aldatıcı siteyi ihbar et”) devre dışı bırak.
policy-DisableForgetButton = Unut düğmesine erişimi engelle.
policy-DisableMasterPasswordCreation = True olarak ayarlanırsa ana parola oluşturulamaz.
policy-DisableProfileImport = Başka uygulamalardan verileri içe aktarmayı sağlayan menü komutunu devre dışı bırak.
policy-DisableSafeMode = Güvenli kipte yeniden başlatma özelliğini devre dışı bırak. Not: Güvenli kipe girmek için kullanılan Shift tuşu, Windows'ta ancak Grup İlkesi ile devre dışı bırakılabilir.
policy-DisableSecurityBypass = Kullanıcının bazı güvenlik uyarılarını atlamasını engelle.
policy-DisableSystemAddonUpdate = { -brand-short-name } uygulamasının sistem eklentilerini yüklemesini ve güncellemesini önle.
policy-DisableTelemetry = Telemetri'yi kapat.
policy-DisplayMenuBar = Varsayılan olarak menü çubuğunu göster.
policy-DNSOverHTTPS = HTTP üzerinden DNS’i yapılandır.
policy-DontCheckDefaultClient = Başlangıçta varsayılan istemci kontrolünü devre dışı bırak.
policy-DownloadDirectory = İndirme klasörünü ayarla ve kilitle.
policy-Proxy = Vekil sunucu ayarlarını yapılandır.
policy-SanitizeOnShutdown2 = Kapatırken gezinti verilerini temizle.
policy-SSLVersionMax = Maksimum SSL sürümünü ayarla.
policy-SSLVersionMin = Minimum SSL sürümünü ayarla.
policy-SupportMenu = Yardım menüsüne özel bir destek öğesi ekle.
