# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# Page title
about-performance-title = Upravitelj zadataka

## Column headers

column-type = Vrsta
column-memory = Memorija

## Special values for the Name column

ghost-windows = Nedavno zatvorene kartice

## Values for the Type column

type-tab = Kartica
type-addon = Dodatak
type-browser = Preglednik

## Values for the Energy Impact column
##
## Variables:
##   $value (Number) - Value of the energy impact, eg. 0.25 (low),
##                     5.38 (medium), 105.38 (high)


## Values for the Memory column
##
## Variables:
##   $value (Number) - How much memory is used


## Tooltips for the action buttons

close-tab =
    .title = Zatvori karticu
