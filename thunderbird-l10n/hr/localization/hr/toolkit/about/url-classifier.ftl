# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

url-classifier-search-result-title = Rezultati
url-classifier-search-result-uri = URI: { $uri }
url-classifier-search-input = URL
url-classifier-search-error-invalid-url = Neispravan URL
url-classifier-search-btn = Započni pretragu
url-classifier-search-features = Značajke
url-classifier-provider-update-btn = Ažuriraj
url-classifier-cache-title = Međuspremnik
url-classifier-cache-refresh-btn = Osvježi
url-classifier-cache-clear-btn = Očisti
url-classifier-trigger-update = Pokreni nadogradnju
url-classifier-not-available = Ništa
url-classifier-disable-sbjs-log = Onemogući JS zapis sigurnog pretraživanja
url-classifier-enable-sbjs-log = Omogući JS zapis sigurnog pretraživanja
url-classifier-enabled = Omogućeno
url-classifier-disabled = Onemogućeno
url-classifier-updating = ažuriranje
url-classifier-cannot-update = nije moguće ažurirati
url-classifier-success = uspjeh
url-classifier-update-error = greška prilikom ažuriranja ({ $error })
url-classifier-download-error = greška prilikom preuzimanja ({ $error })
