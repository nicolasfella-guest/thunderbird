# This Source Code Form is subject to the terms of the Mozilla Public
# License, v- 2-0- If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla-org/MPL/2-0/-

crash-reports-title = Izvještaji rušenja
delete-button-label = Očisti sve
delete-confirm-title = Jeste li sigurni?
delete-unsubmitted-description = Ovo će obrisati sve neposlane izvještaje i oni više neće biti dostupni.
delete-submitted-description = Uvim ćete ukloniti popis poslanih izvještaja rušenja, ali nećete obrisati poslane podatke. Ova radnja se nemože poništiti.
crashes-unsubmitted-label = Neposlani izvještaji rušenja
id-heading = ID izvještaja
date-crashed-heading = Datum rušenja
submit-crash-button-label = Pošalji
# This text is used to replace the label of the crash submit button
# if the crash submission fails.
submit-crash-button-failure-label = Neuspješno
crashes-submitted-label = Poslani izvještaji rušenja
date-submitted-heading = Datum slanja
view-crash-button-label = Prikaži
no-reports-label = Nema poslanih izvještaja rušenja.
no-config-label = Ovaj program nije podešen za prikazivanje izvještaja rušenja. Treba omogućiti značajku <code>breakpad.reportURL</code>.
