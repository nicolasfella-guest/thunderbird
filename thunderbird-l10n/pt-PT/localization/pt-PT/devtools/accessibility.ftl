# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### These strings are used inside the Accessibility panel.

accessibility-learn-more = Saber mais
accessibility-text-label-header = Etiquetas e nomes de texto

## Text entries that are used as text alternative for icons that depict accessibility isses.

accessibility-warning =
    .alt = Aviso
accessibility-fail =
    .alt = Erro
accessibility-best-practices =
    .alt = Melhores práticas

## Text entries for a paragraph used in the accessibility panel sidebar's checks section
## that describe that currently selected accessible object has an accessibility issue
## with its text label or accessible name.

accessibility-text-label-issue-area = Utilize o atributo <code>alt</code> para rotular elementos <div>area</ div> que possuem o atributo <span>href</span>. <a>Saber mais</a>
accessibility-text-label-issue-dialog = Diálogos devem ser rotulados. <a>Saber mais</a>
accessibility-text-label-issue-document-title = Documentos devem ter um <code>title</code>. <a>Saber mais</a>
accessibility-text-label-issue-embed = Conteúdo embutido deve ser rotulado. <a>Saber mais</a>
accessibility-text-label-issue-figure = Figuras com legendas opcionais devem ser rotuladas. <a>Saber mais</a>
accessibility-text-label-issue-fieldset = Elementos <code>fieldset</code> devem ser rotulados. <a>Saber mais</a>
accessibility-text-label-issue-fieldset-legend = Utilize o elemento <code>legend</code> para rotular elementos <span>fieldset</span>. <a>Saber mais</a>
accessibility-text-label-issue-form = Elementos de formulário devem ser rotulados. <a>Saber mais</a>
accessibility-text-label-issue-form-visible = Elementos de formulário devem ter um rótulo de texto visível. <a>Saber mais</a>
accessibility-text-label-issue-frame = Elementos <code>frame</code> devem ser rotulados. <a>Saber mais</a>
accessibility-text-label-issue-glyph = Utilize o atributo <code>alt</ code> para rotular elementos <span>mglyph</span>. <a>Saber mais</a>
accessibility-text-label-issue-heading = Cabeçalhos devem ser rotulados. <a>Saber mais</a>
accessibility-text-label-issue-heading-content = Cabeçalhos devem ter conteúdo de texto visível. <a>Saber mais</a>
accessibility-text-label-issue-iframe = Utilize o atributo <code>title</code> para descrever conteúdo <span>iframe</span>. <a>Saber mais</a>
accessibility-text-label-issue-image = Conteúdo com imagens deve ser rotulado. <a>Saber mais</a>
accessibility-text-label-issue-interactive = Elementos interativos devem ser rotulados. <a>Saber mais</a>
accessibility-text-label-issue-optgroup-label = Utilize o atributo <code>label</code> para rotular elementos <span>optgroup</span>. <a>Saber mais</a>
accessibility-text-label-issue-toolbar = Barras de ferramentas devem ser rotuladas quando houver mais de uma barra de ferramentas. <a>Saber mais</a>
