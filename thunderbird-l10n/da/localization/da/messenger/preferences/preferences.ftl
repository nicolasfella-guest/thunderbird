# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

pane-general-title = Generelt
category-general =
    .tooltiptext = { pane-general-title }
general-language-and-appearance-header = Sprog og udseende
general-incoming-mail-header = Indgående meddelelser
general-files-and-attachment-header = Filer og vedhæftninger
general-tags-header = Mærker
general-reading-and-display-header = Læsning & visning
general-updates-header = Opdateringer
general-network-and-diskspace-header = Netværk & diskplads
general-indexing-label = Indeksering
composition-category-header = Skrivning
composition-attachments-header = Vedhæftede filer
composition-spelling-title = Stavning
compose-html-style-title = HTML-stil
composition-addressing-header = Adresser
privacy-main-header = Privatliv
privacy-passwords-header = Adgangskoder
privacy-junk-header = Spam
privacy-data-collection-header = Dataindsamling og -brug
privacy-security-header = Sikkerhed
privacy-scam-detection-title = Svindelmails
privacy-anti-virus-title = Antivirus
privacy-certificates-title = Certifikater
chat-pane-header = Chat
chat-status-title = Status
chat-notifications-title = Beskeder
chat-pane-styling-header = Stil
choose-messenger-language-description = Vælg det sprog, der skal bruges i brugerfladen i { -brand-short-name }.
manage-messenger-languages-button =
    .label = Vælg alternativer…
    .accesskey = l
confirm-messenger-language-change-description = Genstart { -brand-short-name } for at anvende ændringerne
confirm-messenger-language-change-button = Anvend og genstart
update-pref-write-failure-title = Skrivefejl
# Variables:
#   $path (String) - Path to the configuration file
update-pref-write-failure-message = Kunne ikke gemme indstillingerne. Kan ikke skrive til filen: { $path }
update-setting-write-failure-title = Kunne ikke gemme indstillinger for opdatering
# Variables:
#   $path (String) - Path to the configuration file
# The newlines between the main text and the line containing the path is
# intentional so the path is easier to identify.
update-setting-write-failure-message = { -brand-short-name } stødte på en fejl og gemte ikke ændringen. Bemærk, at for at kunne gemme ændringer, skal der være tilladelse til at skrive til den nedennævnte fil. Du eller en systemadministrator kan måske løse problemet ved at give gruppen Users fuld kontrol over filen.
update-in-progress-title = Opdaterer…
update-in-progress-message = Skal { -brand-short-name } fortsætte med denne opdatering?
update-in-progress-ok-button = &Annuller
# Continue is the cancel button so pressing escape or using a platform standard
# method of closing the UI will not discard the update.
update-in-progress-cancel-button = &Fortsæt
