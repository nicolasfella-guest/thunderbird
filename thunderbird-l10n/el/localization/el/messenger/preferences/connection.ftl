# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

connection-dns-over-https-url-resolver = Χρήση παρόχου
    .accesskey = ρ
# Variables:
#   $name (String) - Display name or URL for the DNS over HTTPS provider
connection-dns-over-https-url-item-default =
    .label = { $name } (Προεπιλογή)
    .tooltiptext = Χρήση προεπιλεγμένου URL για επίλυση DNS αντί HTTPS
connection-dns-over-https-url-custom =
    .label = Προσαρμοσμένο
    .accesskey = Π
    .tooltiptext = Εισάγετε το προτιμώμενο URL για επίλυση DNS αντί HTTPS
connection-dns-over-https-custom-label = Προσαρμοσμένο
