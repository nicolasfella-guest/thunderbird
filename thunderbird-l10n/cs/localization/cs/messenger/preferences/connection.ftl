# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

connection-dns-over-https-url-resolver = Použít poskytovatele
    .accesskey = s
# Variables:
#   $name (String) - Display name or URL for the DNS over HTTPS provider
connection-dns-over-https-url-item-default =
    .label = { $name } (výchozí)
    .tooltiptext = Použít výchozí URL pro DNS přes HTTPS.
connection-dns-over-https-url-custom =
    .label = Vlastní
    .accesskey = V
    .tooltiptext = Zadejte vaší preferovanou URL pro DNS přes HTTPS
connection-dns-over-https-custom-label = Vlastní
