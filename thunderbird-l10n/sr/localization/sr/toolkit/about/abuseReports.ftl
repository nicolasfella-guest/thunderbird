# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

abuse-report-title-extension = Пријави ово проширење продавцу { -vendor-short-name }
abuse-report-title-theme = Пријави ову тему продавцу { -vendor-short-name }
abuse-report-subtitle = У чему је проблем?

## Panel buttons.

abuse-report-cancel-button = Откажи
abuse-report-next-button = Следеће
abuse-report-goback-button = Иди назад
abuse-report-submit-button = Пошаљи

## Message bars descriptions.


## Variables:
##   $addon-name (string) - Name of the add-on

abuse-report-messagebar-submitted = Хвала вам што сте поднели пријаву. Да ли желите уклонити додатак <span data-l10n-name="addon-name">{ $addon-name }</span>?

## Message bars actions.

abuse-report-messagebar-action-remove-extension = Да, уклони
abuse-report-messagebar-action-remove-theme = Да, уклони

## Abuse report reasons (optionally paired with related examples and/or suggestions)

abuse-report-settings-suggestions-search = Промените подразумеване поставке претраге
